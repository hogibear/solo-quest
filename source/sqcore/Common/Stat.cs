﻿namespace Ab.Core.Common
{
    public class Stats
    {
	    public enum Stat
	    {
		    Strength = 0,
			Intelligence = 1,
			Dexterity = 2,
			Constitution = 3,
			Wisdom = 4
	    }
    }
}
