﻿using System.Collections.Generic;
using System.Linq;
using sqcore.Entity;
using sqcore.Items;

namespace sqcore.Common
{
	public static class Repository
	{
		public static IList<Weapon> Weapons => new List <Weapon>
		{
			new Weapon {Id = 1, Name = "Hands", Type = Weapon.WeaponType.Simple, Min = 1, Max = 3, Modifier = 0, ToHit = 0, Range = 0, Ac = 0, IsTwoHanded = false },
			new Weapon {Id = 2, Name = "Dagger", Type = Weapon.WeaponType.Simple, Min = 1, Max = 5, Modifier = 0, ToHit = 0, Range = 0, Ac = 0, IsTwoHanded = false },
			new Weapon {Id = 3, Name = "Short Sword", Type = Weapon.WeaponType.Simple, Min = 1, Max = 6, Modifier = 0, ToHit = 0, Range = 0, Ac = 0, IsTwoHanded = false },
			new Weapon {Id = 4, Name = "Long Sword", Type = Weapon.WeaponType.Sword, Min = 1, Max = 8, Modifier = 0, ToHit = 0, Range = 0, Ac = 0, IsTwoHanded = false },
			new Weapon {Id = 5, Name = "Great Sword", Type = Weapon.WeaponType.Sword, Min = 3, Max = 12, Modifier = 0, ToHit = 0, Range = 0, Ac = 0, IsTwoHanded = true },

			new Weapon {Id = 6, Name = "Hand Axe", Type = Weapon.WeaponType.Axe, Min = 2, Max = 6, Modifier = 0, ToHit = 0, Range = 0, Ac = 0, IsTwoHanded = false },
			new Weapon {Id = 7, Name = "Battle Axe", Type = Weapon.WeaponType.Axe, Min = 2, Max = 8, Modifier = 0, ToHit = 0, Range = 0, Ac = 0, IsTwoHanded = false },
			new Weapon {Id = 8, Name = "Great Axe", Type = Weapon.WeaponType.Axe, Min = 3, Max = 12, Modifier = 0, ToHit = 0, Range = 0, Ac = 0, IsTwoHanded = true },

			new Weapon {Id = 9, Name = "Spear", Type = Weapon.WeaponType.Spear, Min = 2, Max = 7, Modifier = 0, ToHit = 0, Range = 0, Ac = 0, IsTwoHanded = false },
			new Weapon {Id = 10, Name = "War Spear", Type = Weapon.WeaponType.Spear, Min = 2, Max = 10, Modifier = 0, ToHit = 0, Range = 0, Ac = 0, IsTwoHanded = false },
			new Weapon {Id = 11, Name = "War Lance", Type = Weapon.WeaponType.Spear, Min = 3, Max = 14, Modifier = 0, ToHit = 0, Range = 0, Ac = 0, IsTwoHanded = false },

			new Weapon {Id = 12, Name = "Short Bow", Type = Weapon.WeaponType.Bow, Min = 1, Max = 6, Modifier = 0, ToHit = 0, Range = 1, Ac = 0, IsTwoHanded = true },
			new Weapon {Id = 13, Name = "Bow", Type = Weapon.WeaponType.Bow, Min = 2, Max = 10, Modifier = 0, ToHit = 0, Range = 1, Ac = 0, IsTwoHanded = true },
			new Weapon {Id = 14, Name = "Long Bow", Type = Weapon.WeaponType.Bow, Min = 2, Max = 10, Modifier = 0, ToHit = 0, Range = 1, Ac = 0, IsTwoHanded = true },

			new Weapon {Id = 15, Name = "Heavy Crossbow", Type = Weapon.WeaponType.Crossbow, Min = 2, Max = 8, Modifier = 0, ToHit = 0, Range = 1, Ac = 0, IsTwoHanded = true },
			new Weapon {Id = 16, Name = "Hand Crossbow", Type = Weapon.WeaponType.Crossbow, Min = 1, Max = 4, Modifier = 0, ToHit = 0, Range = 1, Ac = 0, IsTwoHanded = false },
			new Weapon {Id = 17, Name = "Light Crossbow", Type = Weapon.WeaponType.Crossbow, Min = 1, Max = 6, Modifier = 0, ToHit = 0, Range = 1, Ac = 0, IsTwoHanded = true }
		};

		public static IList<Armour> Armours => new List<Armour>
		{
			new Armour {Id = 20, Type = Armour.ArmourType.Light, Name = "Adventure's Cloths", Ac = 1},
			new Armour {Id = 21, Type = Armour.ArmourType.Light, Name = "Leather", Ac = 3},
			new Armour {Id = 22, Type = Armour.ArmourType.Light, Name = "Studded Leather", Ac = 4},
			new Armour {Id = 23, Type = Armour.ArmourType.Medium, Name = "Chain Mail", Ac = 6},
			new Armour {Id = 24, Type = Armour.ArmourType.Heavy, Name = "Scale Mail", Ac = 8},
			new Armour {Id = 25, Type = Armour.ArmourType.Heavy, Name = "Plate", Ac = 9}
		};

		public static IList<Shield> Shields => new List<Shield>
		{
			new Shield {Id = 30, Name = "Small Shield", Ac = 1}
		};

		public static EntityNpc GetSlime()
		{
			return new EntityNpc { Name = "Slime", Actions = SlimeActions() };
		}

		public static List<NpcAction> SlimeActions()
		{
			var actions = new List<NpcAction>();

			actions.Add(new NpcAction { Name = "Slime Spit", Value = new Dice { Size = 6, Number = 1, Modifier = 0 } });

			return actions;
		}
	}
}
 