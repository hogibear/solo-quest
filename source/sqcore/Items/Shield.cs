﻿namespace sqcore.Items
{
	public class Shield : IItem
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int Ac { get; set; }
	}
}
