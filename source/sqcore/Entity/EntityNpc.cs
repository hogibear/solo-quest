﻿using System.Collections.Generic;

namespace sqcore.Entity
{
	public class EntityNpc : EntityBase
	{

		public List<NpcAction> Actions { get; set; }
	}
}
