﻿using sqcore.Items;
using System.Collections.Generic;
using System.Linq;

namespace sqcore.Entity
{
	public class EntityPc : EntityBase
	{
		public enum Professions
		{
			Fighter,
			Healer,
			Mage
		}

		public Professions Profession { set; get; }
		public int Xp { set; get; }

		public int Ac => 10 + (EArmour?.Ac ?? 0) + (EShield?.Ac ?? 0);

		public IList<IItem> Inventory { set; get; }

		public Weapon EWeapon { set; get; }
		public Armour EArmour { set; get; }
		public Shield EShield { set; get; }

		public void UnequipWeapon()
		{
			if (EWeapon != null)
			{
				Inventory.Add(EWeapon);
				EWeapon = null;
			}
		}

		public void EquipWeapon(Weapon weapon)
		{
			if (EWeapon != null)
			{
				UnequipWeapon();
			}
			if (weapon != null && weapon.IsTwoHanded && EShield != null)
			{
				Inventory.Add(EShield);
				EShield = null;
			}
			EWeapon = weapon;
		}

		public void UnequipArmour()
		{
			if (EArmour != null)
			{
				Inventory.Add(EArmour);
				EArmour = null;
			}
		}

		public void EquipArmour(Armour armour)
		{
			if (EArmour != null)
			{
				UnequipArmour();
			}
			EArmour = armour;
		}

		public void UnequipShield()
		{
			if (EShield != null)
			{
				Inventory.Add(EShield);
				EShield = null;
			}
		}

		public void EquipShield(Shield shield)
		{
			if (EShield != null)
			{
				UnequipShield();
			}
			EShield = shield;
		}

		public IItem FindItemFromInventory(string name)
		{
			return Inventory.FirstOrDefault(item => item.Name == name);
		}
	}
}
