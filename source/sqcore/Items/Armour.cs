﻿namespace sqcore.Items
{
	public class Armour : IItem
	{
		public enum ArmourType
		{
			Cloth,
			Light,
			Medium,
			Heavy
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public ArmourType Type { get; set; }
		public int Ac { get; set; }
	}
}
