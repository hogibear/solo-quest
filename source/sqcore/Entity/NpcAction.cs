﻿using sqcore.Common;

namespace sqcore.Entity
{
    public class NpcAction
    {
        public string Name { get; set; }

        public Dice Value { get; set; }

    }
}
