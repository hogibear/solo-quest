﻿namespace sqcore.Items
{
	public class Weapon : IItem
	{
		public enum WeaponType
		{
			Simple,
			Sword,
			Axe,
			Spear,
			Bow,
			Crossbow
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public WeaponType Type { get; set; }
		public bool IsTwoHanded { get; set; }

		public int Min { get; set; }
		public int Max { get; set; }
		public int Modifier { get; set; }
		public int ToHit { get; set; }
		public int Range { get; set; }
		public int Ac { get; set; }
	}
}
