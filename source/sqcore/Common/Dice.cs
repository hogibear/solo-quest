﻿namespace sqcore.Common
{
	public class Dice
	{
		public int Size { get; set; }
		public int Number { get; set; }
		public int Modifier { get; set; }
	}
}
