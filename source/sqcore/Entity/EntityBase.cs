﻿using System.Collections.Generic;

namespace sqcore.Entity
{
	public class EntityBase
	{
		public string Name { get; set; }
		public int Attack { get; set; }
		public int Defence { get; set; }

		public int MaxHealth { get; set; }
		public int CurrentHealth { get; set; }

		public IList<Condition> Conditions { set; get; }
	}
}
